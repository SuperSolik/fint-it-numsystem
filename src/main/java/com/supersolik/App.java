package com.supersolik;

import java.math.BigInteger;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.println("Input 3 integers: number, num system from, num system to");
        String line = scanner.nextLine();
        String[] values = line.split(" ");

        int a = Integer.parseInt(values[1]);
        int b = Integer.parseInt(values[2]);
        BigInteger x = new BigInteger(values[0], a);

        System.out.println("Result: " + x.toString(b));
    }
}
